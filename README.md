# MusicExporter

**French**

Script pour macOS permettant de créer deux fichiers comprenant l'artiste et le titre en cours de lecture sur Apple Music.
Existe aussi dans une version permettant de créer un seul fichier combinant les deux.

Utilisable sur OBS, Wirecast, et autres (à condition qu'ils puissent lire les fichiers texte).

**English**

Script for macOS to create two files including artist and title currently playing on Apple Music.
Also exists in a version allowing to create a single file combining the two.

Usable on OBS, Wirecast, and others (provided they can read text files).
